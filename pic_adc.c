/* 
 * File:   pic_adc.c
 * Author: Rafael, Urakami
 *
 * Created on March 23, 2018, 2:27 PM
 */

#include "pic_adc.h"

/**************************************************************************************
 * adcInit
 *
 * Initialize the ADC Module with frequency (FOSC/32), CHANNEL 0 (RA0),
 * RA0-RA1 as analog inputs, RA3 as VREF+ and VSS as VREF-.
 * 
 * TODO: Should we make this function configurable for using the ADC module
 * depending the subsystem?
 **************************************************************************************/
void adcInit()
{
    ADCON0 = 0x81;                  //ADC Module Turned ON, FOSC/32, CHANNEL_0 (RA0)
    ADCON1 = 0x85;                  //RA0-RA1 as Analog Input, RA3 as VREF+, VSS as VREF-
    
    T1CKPS1 = 1;    // PS1 and PS0 in high to set the maximum prescaler
    T1CKPS0 = 1;   
    TMR1CS = 0;     // Timer1 Clock Source Select bit 0 = Internal clock (FOSC/4)
}

/**************************************************************************************
 * picAdcRead
 *
 * Reads 1 time the ADC module in the specified channel, if this function is called 
 * continuously the measurements will be taken around every 2ms. The function will
 * return a 2 bytes value with the value of the measurement without conversion.
 * 
 * The valid channels are from 0 to 6 depending the initialization.
 * 
 * Attributes
 * -----------
 *  * channel: Channel number (in decimal) to be read, if this parameter is out of
 *             range the function will return 0
 * 
 * TODO: Should we make this function configurable for using the ADC module
 * depending the subsystem?
 **************************************************************************************/
unsigned int picAdcRead(unsigned char channel)
{
    TMR1IF = 0;             // Clear TIMER1 flag  
    TMR1ON = 1;             // Turn on TIMER1
    TMR1H = 0x00;           // Initialize TIMER1 MSB counter (0x00 - 100ms)
    TMR1L = 0x00;           // Initialize TIMER1 LSB counter (0x00 - 100ms)
    
    if(channel > 7)                 //If Invalid channel selected 
    {
        return 0;                   //Return 0
    }

    ADCON0 &= 0xC5;                 //Clearing the Channel Selection Bits
    ADCON0 |= channel<<3;           //Setting the required Bits
    
    __delay_ms(2);                  //Acquisition time to charge hold capacitor
    
    GO_nDONE = 1;                   //Initializes A/D Conversion
    
    while(GO_nDONE && (TMR1IF == 0));       //Wait for A/D Conversion to complete
                                            //TODO: If ADC does not respond the program will stuck here
    
    if (TMR1IF == 1){TMR1ON = 0; return 0;}     // If time out then finish with MISO in high
                                                // turn off TIMER1 and return 0
    
    return ((ADRESH<<8)+ADRESL);    //Returns 2 bytes result
}
