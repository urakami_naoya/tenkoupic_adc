/* 
 * File:   pic_adc.h
 * Author: Rafael, Urakami
 *
 * Created on March 23, 2018, 2:27 PM
 */

#ifndef TENKOU_PIC_ADC_H
#define	TENKOU_PIC_ADC_H

#define _XTAL_FREQ     20000000

#include <xc.h>
#include <stdio.h>
#include <stdint.h>

// Function declarations.
void adcInit(void);
unsigned int picAdcRead(unsigned char channel);
#endif // End header safeguard.