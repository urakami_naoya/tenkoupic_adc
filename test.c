// PIC16F877 Configuration Bit Settings

// 'C' source line config statements

// CONFIG
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config CP = OFF         // FLASH Program Memory Code Protection bits (Code protection off)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)
#pragma config LVP = OFF        // Low Voltage In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EE Memory Code Protection (Code Protection off)
#pragma config WRT = OFF        // FLASH Program Memory Write Enable (Unprotected program memory may not be written to by EECON control)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
#include <stdio.h>
#include <stdint.h>
#include "pic_adc.h"
#include "uart.h"

void test_adcInit()
{
    adcInit();              //Initializes ADC Module
}

float test_adcRead()        // Reads specified channel on adcRead function
{
    unsigned int a;         // Variable to store the ADC received value
    float v;                // Variable to store converted voltage
    char string_1[10];      // Test string_1
    
    TRISA0 = 1;                 //RA0 as analog input for RSSI measurement
    
    a = picAdcRead(0);          // Reading Analog Channel 0
    v = ((float)a/1024)*2.5;    // Calculating the voltage value in volts
    
    sprintf(string_1, "%.2f%c", v, 0x0D);           // Converting to ASCII values           
    uartWriteBytes(string_1, sizeof (string_1));    // Sending v ASCII value by UART
    
    return v;
}

void main()
{
    uartInit(9600);     //Initialize UART Module 9600bps  
    test_adcInit();     //Initialize ADC module 
    
    for(;;)
    {
        test_adcRead(); // Read and print ADC
        __delay_ms(1000); //Delay
    }//Infinite Loop
}